FROM centos:7

EXPOSE 5290
WORKDIR /app/server

RUN yum -y install python3 python3-pip \
  && yum clean all \
  && rm -rf /var/cache/yum

COPY . .
RUN pip3 install -r requirements.txt

ENTRYPOINT [ "python3", "python-api.py" ]
