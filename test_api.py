import requests
import pytest

BASE_HOST = 'http://localhost:5290'


@pytest.fixture(scope='session')
def get_info_response():
    return requests.get(url=f'{BASE_HOST}/get_info').json()


@pytest.mark.parametrize('param_name, expected_value', [
    ('version', 3),
    ('method', 'GET'),
    ('message', 'Already started'),
])
def test_get_info_method(get_info_response, param_name, expected_value):
    assert get_info_response[param_name] == expected_value, 'error in method'
